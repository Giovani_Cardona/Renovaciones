from PyPDF2 import PdfFileWriter, PdfFileReader
import pandas as pd
import time
import tabula
import datetime
import numpy as np
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import sys
import win32com.client as client
import os
from PyPDF2 import PdfFileMerger
import traceback
import codecs

def read_daily_pdf_autos(pdf):
    date = datetime.datetime.now()
    tablas = tabula.read_pdf(pdf)
    if len(tablas)>0:
        tabula.convert_into(pdf, pdf[:22] + "-" + str(date.day) + "-" + str(date.month) + "-"+ str(date.year) + ".csv", output_format="csv", pages='all')
        file = pd.read_csv(pdf[:22]+ "-" + str(date.day) + "-" + str(date.month) + "-"+ str(date.year) + ".csv",encoding='latin-1',dtype=object)
        file.dropna(inplace=True)
        file['Valor renovación (Iva incluido)'] = file['Valor renovación (Iva incluido)'].apply(lambda x: x.replace('$', '')).apply(lambda x: x.replace(',', '')).astype(int)
        file.reset_index(inplace=True, drop=True)
        return file
    else:
        poliza = []
        placa = []
        asegurado = []
        id_asegurado = []
        val_ren = []
        input1 = PdfFileReader(open(pdf, "rb"))
        output = PdfFileWriter()
        output.addPage(input1.getPage(0))
        page = input1.getPage(0)
        text = page.extractText()
        for j in range(20):
            try:
                text.index("$") >= 0
                subtext = text[:text.index("$") + 15]
                subtext = text[:subtext.rindex(",") + 4]
                text = text[subtext.rindex(",") + 5:]
                if j==0:
                    inicio = subtext.index(")")+1
                    poliza.append(subtext[inicio:inicio + 12])
                else:
                    inicio = 1
                    poliza.append(subtext[inicio - 1:inicio + 12])
                placa.append(subtext[inicio+12:inicio+18])
                ini_id = 0
                for i in range(inicio+19,subtext.index("$"),1):
                    if subtext[i].isdigit() and ini_id == 0:
                        ini_id = i
                asegurado.append(subtext[inicio+18:ini_id])
                id_asegurado.append(subtext[ini_id:subtext.index("$")])
                val_ren.append(subtext[subtext.index("$"):subtext.rindex(",") + 4])

            except:
                j = 20

        file = pd.DataFrame()
        file["Póliza"]=poliza
        file["Placa"]=placa
        file["Asegurado"]=asegurado
        file["IDAsegurado"]=id_asegurado
        file["Valor renovación (Iva incluido)"]=val_ren

        return file

def log_interhelper(driver, user = "",password=""):
    driver.get("https://www.interhelpermail.com/interhelper/index.php")
    if (user == "") or (password == ""):
        time.sleep(30)
        driver.get("https://www.interhelpermail.com/interhelper/app.php#app/contenido.php?no_popups=0&altura=864")

    else:
        time.sleep(5)
        driver.find_element_by_id('email').send_keys(user)
        driver.find_element_by_id('clave1').send_keys(password)
        driver.find_element_by_css_selector('input.btn-lg.btn-block.btn.btn-primary.btn-login').click()
        time.sleep(2)
        driver.find_element_by_css_selector("button.btn.btn-primary").click()
        time.sleep(5)
        driver.get("https://www.interhelpermail.com/interhelper/app.php#app/contenido.php?no_popups=0&altura=864")

    try:
        time.sleep(1)
        driver.switch_to.alert.accept()

    except:
        print("No alert")

    windows_active = driver.window_handles
    try:
        driver.switch_to.window(windows_active[1])
        time.sleep(3)
        driver.close()
        driver.switch_to.window(windows_active[0])
    except:
        print("No window")

    time.sleep(5)

def log_sura_cotizador(driver):
    driver.get("https://cotizadores.sura.com/")
    WebDriverWait(driver, 200).until(
        EC.element_to_be_clickable((By.ID, "dropdownMenuButton")))
    time.sleep(10)

def log_suc_virtual_sura(driver):
    driver.get("https://login.suramericana.com/SSAutenticacion/faces/autenticacion/paginaAutenticacion.jspx?cdApp=SURACOM&cookies=false&ReturnUrl=%2f_layouts%2fAuthenticate.aspx%3fSource%3d%252f&Source=%2f")
    time.sleep(10)
    if driver.find_element_by_id("tempUserID"):
        time.sleep(30)
    else:
        driver.get("https://www.segurossura.com.co/paginas/default.aspx")
        time.sleep(10)
        driver.find_element_by_css_selector("button.dropdown-toggle").click()
        time.sleep(2)
        driver.find_element_by_id("asesores").click()
        time.sleep(4)
        driver.get("https://login.suramericana.com/SSAutenticacion/faces/autenticacion/paginaAutenticacion.jspx?cdApp=SURACOM&cookies=false&ReturnUrl=%2f_layouts%2fAuthenticate.aspx%3fSource%3d%252f&Source=%2f")
        time.sleep(30)
        menu = driver.find_elements_by_css_selector("div.main_navigation > a")
        menu[1].click()
        driver.get("https://asistentevirtual.suramericana.com/IndexPage.aspx")


def get_date_info():
    months = [["Ene", "ENERO", 1],
              ["Feb", "FEBRERO", 2],
              ["Mar", "MARZO", 3],
              ["Abr", "ABRIL", 4],
              ["May", "MAYO", 5],
              ["Jun", "JUNIO", 6],
              ["Jul", "JULIO", 7],
              ["Ago", "AGOSTO", 8],
              ["Sep", "SEPTIEMBRE", 9],
              ["Oct", "OCTUBRE", 10],
              ["Nov", "NOVIEMBRE", 11],
              ["Dic", "DICIEMBRE", 12]]
    date = datetime.datetime.now()
    year = date.strftime("%Y")
    for month_list in months:
        if month_list[2] == date.month:
            month = month_list[0]
            complete_month = month_list[1]
            month_number = date.strftime("%m")
    day = date.strftime("%d")

    return date,year,month,complete_month,day,month_number

def caratulas_analisis(pdf):
    try:
        input1 = PdfFileReader(open(pdf, "rb"))
        tablas = tabula.read_pdf(pdf, pages="all")
        output = PdfFileWriter()
        output.addPage(input1.getPage(0))
        page = input1.getPage(0)
        page2 = input1.getPage(1)
        text = page.extractText()
        text2 = page2.extractText()
        pos_prima = text.lower().find("total a pagar")
        valor_prima=""
        if pos_prima >=0:

            for i in range(pos_prima,pos_prima+30,1):
                if text[i].isdigit():
                    valor_prima = valor_prima + text[i]
            valor_prima = int(valor_prima)
        else:
            valor_prima = "No se encontró"

        df1 = tablas[0]
        if df1.empty == False:

            try:
                valor_asegurado = int(df1[df1.columns[2]][4].replace("$", "").replace(",", "").replace(" ", ""))
                ciudad_circulacion = df1["Clase"][2]
                fasecolda = df1[df1.columns[1]][2]

            except:
                try:
                    valor_asegurado = int(df1[df1.columns[2]][6].replace("$", "").replace(",", "").replace(" ", ""))
                    ciudad_circulacion = df1["Clase"][4]
                    fasecolda = df1[df1.columns[1]][4]
                except:
                    valor_asegurado = "No se encontró"
                    ciudad_circulacion = "No se encontró"
                    fasecolda = "No se encontró"


            if text.lower().find("global") >= 0:
                producto = "Plan Autos Global"
            elif text.lower().find("clsico") >= 0:
                producto = "Plan Autos Clásico"
            else:
                producto = "Verificar plan"

            df2 = tablas[2]

            try:
                deducibles_index = df2[(df2[df2.columns[0]] == "Pérdida Total")].index
                if deducibles_index[0] == 6:
                    if df2[df2.columns[1]].isnull()[4] and text2.lower().find("daos a terceros") >= 0:
                        ded_terceros = df2[df2.columns[1]][3].replace("$", "")

                    elif text2.lower().find("daos a terceros") >= 0:
                        ded_terceros = df2[df2.columns[1]][4].replace("$", "")

                    else:
                        ded_terceros = "Revisar daños a terceros"

                    if df2[df2.columns[1]].isnull()[deducibles_index[0] + 1] and text2.lower().find("daos al carro") >= 0:
                        ded_da_car = df2[df2.columns[1]][deducibles_index[0]].replace("$", "")

                    elif text2.lower().find("daos al carro") >= 0:
                        ded_da_car = df2[df2.columns[1]][deducibles_index[0] + 1].replace("$", "")

                    else:
                        ded_da_car = "Revisar daños a carro"

                    try:
                        if df2[df2.columns[1]].isnull()[deducibles_index[1] + 1] and text2.lower().find("hurto") >= 0:
                            ded_hu_car = df2[df2.columns[1]][deducibles_index[1]].replace("$", "")

                        elif text2.lower().find("hurto") >= 0:
                            ded_hu_car = df2[df2.columns[1]][deducibles_index[1] + 1].replace("$", "")

                        else:
                            ded_hu_car = "Revisar hurtos a terceros"
                    except:
                        if df2[df2.columns[1]].isnull()[17] and text2.lower().find("hurto") >= 0:
                            ded_hu_car = df2[df2.columns[1]][16].replace("$", "")

                        elif text2.lower().find("hurto") >= 0:
                            ded_hu_car = df2[df2.columns[1]][17].replace("$", "")

                        else:
                            ded_hu_car = "Revisar hurtos a terceros"

                elif deducibles_index[0] == 7:
                    if df2[df2.columns[1]].isnull()[4] and text2.lower().find("daos a terceros") >= 0:
                        ded_terceros = df2[df2.columns[1]][3].replace("$", "")

                    elif text2.lower().find("daos a terceros") >= 0:
                        ded_terceros = df2[df2.columns[1]][4].replace("$", "")

                    else:
                        ded_terceros = "Revisar daños a terceros"

                    if df2[df2.columns[1]].isnull()[deducibles_index[0]] and text2.lower().find("daos al carro") >= 0:
                        ded_da_car = df2[df2.columns[1]][deducibles_index[0] - 1].replace("$", "")

                    elif text2.lower().find("daos al carro") >= 0:
                        ded_da_car = df2[df2.columns[1]][deducibles_index[0]].replace("$", "")

                    else:
                        ded_da_car = "Revisar daños a carro"

                    try:
                        if df2[df2.columns[1]].isnull()[deducibles_index[1]] and text2.lower().find("hurto") >= 0:
                            ded_hu_car = df2[df2.columns[1]][deducibles_index[1] - 1].replace("$", "")

                        elif text2.lower().find("hurto") >= 0:
                            ded_hu_car = df2[df2.columns[1]][deducibles_index[1]].replace("$", "")

                        else:
                            ded_hu_car = "Revisar hurtos a terceros"
                    except:
                        if df2[df2.columns[1]].isnull()[17] and text2.lower().find("hurto") >= 0:
                            ded_hu_car = df2[df2.columns[1]][16].replace("$", "")

                        elif text2.lower().find("hurto") >= 0:
                            ded_hu_car = df2[df2.columns[1]][17].replace("$", "")

                        else:
                            ded_hu_car = "Revisar hurtos a terceros"

            except:
                try:
                    df2 = tablas[11]
                    deducibles_index = df2[(df2[df2.columns[1]] == "Limite\rDeducible")].index
                    ded_terceros = df2[df2.columns[2]][deducibles_index[0]].replace("$", "")
                    deducibles_index = df2[(df2[df2.columns[1]] == "Pérdida Total\rPérdida Parcial\rGastos de Transporte")].index
                    ded_da_car = df2[df2.columns[2]][deducibles_index[0]][:df2[df2.columns[2]][deducibles_index[0]].index("\r")].replace("$", "")
                    ded_hu_car = df2[df2.columns[2]][deducibles_index[1]][:df2[df2.columns[2]][deducibles_index[0]].index("\r")].replace("$", "")
                except:
                    ded_terceros = "Error"
                    ded_da_car = "Error"
                    ded_hu_car = "Error"


            bancos = listado_bancos()
            found = False
            hon_ben = ""

            for i in range(len(bancos)):
                if text.lower().find(bancos[i][0].lower()) >= 0 and found == False:
                    hon_ben = bancos[i][0]
                    found = True

            data = {
                "prima":valor_prima,
                "Fasecolda":fasecolda,
                "Valor Asegurado 2019":valor_asegurado,
                "Deducible daño a terceros":ded_terceros,
                "Deducible daños al carro":ded_da_car,
                "Deducible hurto al carro":ded_hu_car,
                "Producto": producto,
                "Ciudad de circulación": ciudad_circulacion,
                "Beneficiario honeroso": hon_ben
            }

            return data

        else:
            fasecolda = ""
            pos_fasecolda = text.lower().find("fasecolda")
            text_help = text[pos_fasecolda:pos_fasecolda + 100]

            for i in range(len(text_help)):

                if text_help[i].isdigit() and len(fasecolda)<8:
                    fasecolda = fasecolda + text_help[i]


            pos_val_aseg = text.lower().find("valor total asegurado")
            text_help = text[pos_val_aseg + 22:pos_val_aseg + 60]
            valor_asegurado = text_help[text_help.find("$")+1:text_help.rfind(",")+4].strip()
            text_help = text2[text2.find("Limite"):text2.find("Limite") + 100]

            ded_terceros=text_help[text_help.find("$"):text_help.find("$")+4].replace("$","").strip()
            text_help = text2[text2.lower().find("daos al carro"):text2.lower().find("daos al carro")+100]
            if text_help.find("%"):
                ded_da_car = text_help[text_help.find("%")-2:text_help.find("%")+1].strip()
            else:
                ded_da_car = text_help[text_help.find("$"):text_help.find("$")+4].replace("$","").strip()
            text_help = text2[text2.lower().find("hurto al carro"):text2.lower().find("hurto al carro") + 100]
            ded_hu_car = text_help[text_help.find("$"):text_help.find("$") + 4].replace("$", "").strip()

            if text.lower().find("global") >= 0:
                producto = "Plan Autos Global"
            elif text.lower().find("clsico") >= 0:
                producto = "Plan Autos Clásico"
            else:
                producto = "Verificar plan"
            text_help = text[text.lower().find("ciudad de circulacin"):text.lower().find("valor de referencia")]
            for i in range(len(text_help) - 1, 0, -1):
                if text_help[i].isdigit():
                    pos_ci_cir = i
                    break
            ciudad_circulacion = text_help[pos_ci_cir + 1:len(text_help)]
            bancos = listado_bancos()
            found = False
            hon_ben = ""

            for i in range(len(bancos)):
                if text.lower().find(bancos[i][0].lower()) >= 0 and found == False:
                    hon_ben = bancos[i][0]
                    found = True

            data = {
                "prima":valor_prima,
                "Fasecolda":fasecolda,
                "Valor Asegurado 2019":valor_asegurado,
                "Deducible daño a terceros":ded_terceros,
                "Deducible daños al carro":ded_da_car,
                "Deducible hurto al carro":ded_hu_car,
                "Producto": producto,
                "Ciudad de circulación": ciudad_circulacion,
                "Beneficiario honeroso": hon_ben
            }

            return data

    except:
        data = {
            "prima": "Error",
            "Fasecolda": "Error",
            "Valor Asegurado 2019": "Error",
            "Deducible daño a terceros": "Error",
            "Deducible daños al carro": "Error",
            "Deducible hurto al carro": "Error",
            "Producto": "Error",
            "Ciudad de circulación": "Error",
            "Beneficiario honeroso": sys.exc_info()
        }

        return data


def listado_bancos():
    bancos =[["BANCOLOMBIA S.A",8909039388],
             ["BANCO DE BOGOTA S.A" , 8600029644],
             ["BANCO DAVIVIENDA S.A",8600343137],
             ["BANCO DE OCCIDENTE S.A",8903002794],
             ["BBVA COLOMBIA",8600030201],
             ["BANCO PICHINCHA S.A",8902007567],
             ["SCOTIABANK COLPATRIA S.A",8600345941],
             ["BANCO FINANDINA",8600518946],
             ["ITAU CORPBANCA COLOMBIA SA",8909039370],
             ["RCI COLOMBIA S.A.COMPAÑIA DE FINANCIAMIENTO",9009776291],
             ["CHEVYPLAN S A SOCIEDAD ADMINISTRADORA DE PLANES DE AUTOFINANCIAMIENTO COMERCIAL",8300011337],
             ["PLANAUTOS S A",8909240764],
             ["GMAC  FINANCIERA",8600293968],
             ["VEHIGRUPO SAS",9004851691],
             ["MAF COLOMBIA S.A.S",9008397029],
             ["BANCO CAJA SOCIAL S.A.",8600073354]]

    return bancos



def cotizador_sura_renovaciones(driver,file,year,value_past_year):
    ######### Definición de id's ###############
    principal_button_menu = "dropdownMenuButton"  ### Botón de menú desplegable
    inputs_id = "input"  ### Botones de input
    elec_sol = "solicitudElectronica"  ### Pestaña de solicitud electrónica
    term_cond = "checkbox"  ### Checkbox para aceptar terminos y condiciones

    ######### Definición de selectores ############

    solutions_item = "a.dropdown-item.soluciones"  ### Item de soluciones, botón para abrir el primer menú desplegable
    menus = "a.dropdown-item"  ### selector del primer menú despleglable
    consult_button = "paper-button.boton-accion-principal.x-scope.paper-button-0"  ### Botón para realizar la consulta de la poliza a renovar

    ######### Definición de xpath's ############

    acc_to_occ = "//label[@id='paper-input-label-63']"  ### label de accidentes a Ocupantes
    val_ato = "//paper-item[text()='35.000.000/OCUPANTE']"  ### valor de accidentes a ocupantes en plan global

    for poliza in file['Póliza']:

        try:
            driver.get("https://cotizadores.sura.com/")
            try:
                time.sleep(1)
                driver.switch_to.alert.accept()
            except:
                print("No alert")
            time.sleep(10)
            x = 0
            while x == 0:
                try:
                    driver.find_element_by_id(principal_button_menu).click()
                    x = 1
                except:
                    time.sleep(10)
            time.sleep(1)
            driver.find_element_by_css_selector(solutions_item).click()
            time.sleep(1)
            menu = driver.find_elements_by_css_selector(menus)
            time.sleep(1)
            menu[1].click()
            time.sleep(1)
            second_menu = driver.find_elements_by_css_selector(menus)
            time.sleep(1)
            second_menu[4].click()
            x = 0

            while x < 20:
                try:
                    inputs = driver.find_elements_by_id(inputs_id)
                    inputs[0].send_keys(str(poliza))
                    inputs[2].send_keys("RENOVACIÓN " + year)
                    time.sleep(1)
                    driver.find_element_by_css_selector(consult_button).click()
                    time.sleep(10)
                    x = 21
                except:
                    print("error " + str(x))
                    time.sleep(5)
                    x = x + 1

            driver.find_element_by_xpath(acc_to_occ).click()
            time.sleep(2)
            driver.find_element_by_xpath(val_ato).click()

            ### driver.find_element_by_id(elec_sol).click()
            ### driver.find_element_by_id(term_cond).click()
            ### driver.find_elements_by_css_selector("paper-button.boton-accion-principal.x-scope.paper-button-0").click()




        except:

            print("Error en la póliza: " + str(poliza))
            value_past_year.append("Error")

def convert_to_pdf(filepath):
    """Save a pdf of a docx file."""
    try:
        word = client.DispatchEx("Word.Application")
        target_path = filepath.replace(".docx", r".pdf")
        word_doc = word.Documents.Open(filepath)
        word_doc.SaveAs(target_path, FileFormat=17)
        word_doc.Close()
        os.remove(filepath)
    except Exception as e:
            raise e
    finally:
            word.Quit()

def get_consecutive(archivo):

    codigos = pd.read_excel(archivo)
    subset_codigos = codigos.dropna(subset=['EMPRESA'])
    subset_codigos.reset_index(drop=True,inplace=True)
    new = subset_codigos.iloc[:,1]
    position = (codigos.index[codigos['CONSECUTIVO FACTURACION MANUAL '] == subset_codigos['CONSECUTIVO FACTURACION MANUAL '][len(new)-1]].tolist()[0])+1

    return codigos['CONSECUTIVO FACTURACION MANUAL '][position] , position+2



"""
Renovaciones = pd.read_excel("Renovaciones.xlsx")

driver = webdriver.Chrome("D:\Selenium\chromedriver.exe")
driver.maximize_window()


for i in Renovaciones['Póliza']:
    driver.get("https://cotizadores.sura.com/")
    time.sleep(10)
    x = 0
    while x == 0:

        try:
            driver.find_element_by_id("dropdownMenuButton").click()
            x = 1
        except:
            time.sleep(10)
    time.sleep(1)
    driver.find_element_by_css_selector("a.dropdown-item.soluciones").click()
    time.sleep(1)
    menu = driver.find_elements_by_css_selector("a.dropdown-item")
    time.sleep(1)
    menu[1].click()
    time.sleep(1)
    second_menu = driver.find_elements_by_css_selector("a.dropdown-item")
    time.sleep(1)
    second_menu[4].click()
    time.sleep(10)
    inputs = driver.find_elements_by_id("input")
    inputs[0].send_keys(i) 
"""""

def city_name(codigo):
    dep = codigo[:2]
    mun = codigo[2:]
    file = pd.read_csv('ciudades.csv',delimiter=";")
    filt = file[file['Código Municipio'] == int(mun)]
    municipio = str(filt[filt['Código Departamento'] == int(dep)]['Nombre Municipio'][0])
    return municipio

def join_pdfs(pdfs,output_file):
    fusionador = PdfFileMerger()

    for pdf in pdfs:
        fusionador.append(open(pdf, 'rb'))
    try:
        with open(output_file, 'wb') as salida:
            fusionador.write(salida)
    except:
        with open(output_file.replace(".pdf","(1).pdf"), 'wb') as salida:
            fusionador.write(salida)

def get_vencimiento(vigencia):
    vigencia = vigencia.lower()
    words = vigencia.split()
    meses = [["enero","01"],
             ["febrero","02"],
             ["marzo","03"],
             ["abril","04"],
             ["mayo","05"],
             ["junio","06"],
             ["julio","07"],
             ["agosto","08"],
             ["septiembre","09"],
             ["octubre","10"],
             ["noviembre","11"],
             ["diciembre","12"]]
    tuple = []
    for mes in meses:
        if mes[0] in words:
            tuple.append([words.index(mes[0]),mes[1]])
    day = tuple[0][0]-2

    vencimiento = str(words[day]) + "/" + str(tuple[0][1]) + "/" + str(datetime.datetime.now().year)
    vencimiento = datetime.datetime.strftime(datetime.datetime.strptime(vencimiento,"%d/%m/%Y") + datetime.timedelta(days=10),"%d/%m/%Y")

    return vencimiento

def get_greeting():
    hour = datetime.datetime.now().hour
    if hour < 12:
        greeting = "Buenos días "
    else:
        greeting = "Buenas tardes "

    return greeting

def error_mail(subject="",error="", body=""):
    outlook = client.Dispatch('outlook.application')
    mail = outlook.CreateItem(0)
    mail.To = "sebastian.narvaez@grupobootes.com"
    mail.Subject = 'Error: ' + error
    mail.Body = body + "\n\nFecha de ejecución: " + datetime.datetime.now().strftime("%d/%m/%Y") + "\nHora de ejecución: " + datetime.datetime.now().strftime("%H:%M:%S")
    st = os.path.join(os.getcwd(), "error_log.txt")
    mail.Attachments.Add(st)
    mail.Send()
    time.sleep(7)

def log_error(error):
    try:
        line = datetime.datetime.now().strftime("%d/%m/%Y:%H:%M:%S") + "________" + str(error[1]) + " in line " + str(error[2].tb_lineno)
    except:
        line = datetime.datetime.now().strftime("%d/%m/%Y:%H:%M:%S") + "________" + error
    with open("error_log.txt","a") as file:
        file.write("\n" + line)

def mail_body(mail,greeting, mail_name,consecutivo,ramo,vigencia,valor_total):
    logo = os.path.join(os.getcwd(),'Plantillas','a&s - mailing 2','img',"ays.png")
    #fondo_ays = os.path.join(os.getcwd(),'Plantillas','a&s - mailing 2','img',"fondo-ays.png")
    fondo_azul = os.path.join(os.getcwd(),'Plantillas','a&s - mailing 2','img',"fondo__azul.png")
    papa_e_hija = os.path.join(os.getcwd(),'Plantillas','a&s - mailing 2','img',"papa-e-hija.png")
    facebook = os.path.join(os.getcwd(),'Plantillas','a&s - mailing 2','img',"facebook.png")
    instagram = os.path.join(os.getcwd(),'Plantillas','a&s - mailing 2','img',"instagram.png")
    linkedin = os.path.join(os.getcwd(),'Plantillas','a&s - mailing 2','img',"linkedin.png")

    logo_attach = mail.Attachments.Add(logo)
    #fondo_ays_attach = mail.Attachments.Add(fondo_ays)
    fondo_azul_attach = mail.Attachments.Add(fondo_azul)
    papa_e_hija_attach = mail.Attachments.Add(papa_e_hija)
    facebook_attach = mail.Attachments.Add(facebook)
    instagram_attach = mail.Attachments.Add(instagram)
    linkedin_attach = mail.Attachments.Add(linkedin)

    logo_attach.PropertyAccessor.SetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001F",
                                             "MyId1")
    #fondo_ays_attach.PropertyAccessor.SetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001F",
     #                                             "MyId2")
    fondo_azul_attach.PropertyAccessor.SetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001F",
                                                   "MyId3")
    papa_e_hija_attach.PropertyAccessor.SetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001F",
                                                    "MyId4")
    facebook_attach.PropertyAccessor.SetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001F",
                                                    "MyId5")
    instagram_attach.PropertyAccessor.SetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001F",
                                                    "MyId6")
    linkedin_attach.PropertyAccessor.SetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001F",
                                                    "MyId7")

    html = codecs.open(os.path.join(os.getcwd(),'Plantillas','a&s - mailing 2','index.html'),'r','utf-8')
    html_string = html.read()

    mail_body = html_string
    mail_body = mail_body.replace("NOMBRE",mail_name).replace("Estimado (a)",greeting).replace("[RAMO]",ramo).replace("[MES]",vigencia).replace("[YYYYYYYY]",str(consecutivo)).replace("[XXXXXXXX]","$ " + '{:,.0f}'.format(
                    int(valor_total)).replace(",", "@").replace(".", ",").replace("@", "."))
    return mail_body





def validation(file):

    validation = True
    if str(file.iloc[0][0]).lower().strip() == "nombre empresa" and str(file.iloc[1][0]).lower().strip() == "nombre ramo" and str(file.iloc[2][0]).lower().strip() == "número póliza" and str(file.iloc[3][0]).lower().strip() == "número documento" and str(file.iloc[4][0]).lower().strip() == "vigencia" and str(file.iloc[5][0]).lower().strip() == "compañía de seguros":
        sub_file = pd.DataFrame(file.iloc[0:6])

        for i in range(len(sub_file)):
            if str(sub_file.iloc[i, 1]) == "nan":
                validation = False
                print("El campo: " + str(sub_file.iloc[i, 0]).strip() + ", se encuentra vacio")
                time.sleep(100)
        if validation:
            if str(file.iloc[8:0]) == "nan":
                validation = False
                print("No se encuentra inicio de la tabla, recuerda que el inicio de la tabla es en la fila 8")
                time.sleep(100)

    else:
        validation = False
        print("Error en la estructura")
        print("Verificar que el nombre de la empresa, ramo, número de póliza, número de documento, vigencia y compañía de seguros esten nombrados correctamente")
        print("Deben aparecer en la primera columna del excel con estos nombres exactos y en este orden: \n")
        print("NOMBRE EMPRESA \n NOMBRE RAMO \n NÚMERO PÓLIZA \n NÚMERO DOCUMENTO \n VIGENCIA \n COMPAÑÍA DE SEGUROS")
        time.sleep(100)

    return validation

def column_validation(file_col,template_file_col):
    validation = True
    file_col = list(file_col)
    template_file_col = list(template_file_col)

    try:
        file_col.remove("Email")
    except:
        print("No se encuentra la columna Email")
        validation = False
        time.sleep(100)

    try:
        file_col.remove("Facturar a")
    except:
        print("No se encuentra la columna Facturar a")
        validation = False
        time.sleep(100)

    try:
        file_col.remove("Nombre correo")
    except:
        print("No se encuentra la columna Nombre correo")
        validation = False
        time.sleep(100)


    return validation

def extract_date(date):
    months = [["enero", 1],
              ["febrero", 2],
              ["marzo", 3],
              ["abril", 4],
              ["mayo", 5],
              ["junio", 6],
              ["julio", 7],
              ["agosto", 8],
              ["septiembre", 9],
              ["octubre", 10],
              ["noviembre", 11],
              ["diciembre", 12]]
    a = date.split("-")
    day = a[2]
    number_month = a[1]
    year = a[0]

    for month in months:
        if month[1]==int(number_month):
            name_month = month[0]

    return day, name_month, year


def mail_body_pre_renovacion(mail,greeting,mail_name,vigencia,placa):
    day, name_month, year = extract_date(vigencia)
    logo = os.path.join(os.getcwd(),'Plantillas','a&s - mailing 1','img',"ays.png")
    #fondo_ays = os.path.join(os.getcwd(),'Plantillas','a&s - mailing 1','img',"fondo-ays.png")
    fondo_azul = os.path.join(os.getcwd(),'Plantillas','a&s - mailing 1','img',"fondo__azul.png")
    ays_banner = os.path.join(os.getcwd(),'Plantillas','a&s - mailing 1','img',"ays-banner.jpg")
    facebook = os.path.join(os.getcwd(),'Plantillas','a&s - mailing 1','img',"facebook.png")
    instagram = os.path.join(os.getcwd(),'Plantillas','a&s - mailing 1','img',"instagram.png")
    linkedin = os.path.join(os.getcwd(),'Plantillas','a&s - mailing 1','img',"linkedin.png")

    logo_attach = mail.Attachments.Add(logo)
    #fondo_ays_attach = mail.Attachments.Add(fondo_ays)
    fondo_azul_attach = mail.Attachments.Add(fondo_azul)
    ays_banner_attach = mail.Attachments.Add(ays_banner)
    facebook_attach = mail.Attachments.Add(facebook)
    instagram_attach = mail.Attachments.Add(instagram)
    linkedin_attach = mail.Attachments.Add(linkedin)

    logo_attach.PropertyAccessor.SetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001F",
                                             "MyId1")
    #fondo_ays_attach.PropertyAccessor.SetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001F",
     #                                             "MyId2")
    fondo_azul_attach.PropertyAccessor.SetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001F",
                                                   "MyId3")
    ays_banner_attach.PropertyAccessor.SetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001F",
                                                    "MyId4")
    facebook_attach.PropertyAccessor.SetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001F",
                                                    "MyId5")
    instagram_attach.PropertyAccessor.SetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001F",
                                                    "MyId6")
    linkedin_attach.PropertyAccessor.SetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001F",
                                                    "MyId7")

    html = codecs.open(os.path.join(os.getcwd(),'Plantillas','a&s - mailing 1','index.html'),'r','utf-8')
    html_string = html.read()

    mail_body = html_string
    mail_body = mail_body.replace("NOMBRE",mail_name).replace("Estimado (a)",greeting).replace("%%PLACA%%",placa).replace("%%FECHA_VENC%%",vigencia)



    return mail_body


def mail_body_renovacion(greeting,mail_name,cantidad,polizas,mes):

    

    html = codecs.open(os.path.join(os.getcwd(),'Plantillas','a&s - mailing 1','index.html'),'r','utf-8')
    html_string = html.read()

    mail_body = html_string
    mail_body = mail_body.replace("%%saludo%%",greeting).replace("%%NOMBRE%%",mail_name).replace("%%s%%",cantidad).replace("%%POLIZAS%%",polizas).replace("%%MES%%",mes)



    return mail_body

def dataOut(file,x):
    
    DataOut=open(file,'a')
    DataOut.write(x)
    DataOut.write('\n')
    DataOut.close