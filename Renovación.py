import time
import os
import datetime
import openpyxl
import pandas as pd
import docx
import sys
import Support as sp
import win32com.client as client
from openpyxl.styles import numbers



################# Nombramiento de archivos ###############################
                                                                        ##
archivo_renovaciones = "A&S_Renovaciones.xlsx"                         ##
current_month = 'Octubre 2020'
Comercial = ['ZAPATA LONDONO ANGELA MARIA','MUNOZ RAMIREZ  KATHERINE'] #,'DUQUE BRAN ANA MARIA '
Operacion = 'Renovación'

##
##########################################################################

try:


    ##################### Nombramiento hoja actual ###########################
                                                                            ##
    date,year,month,complete_month,day,month_number = sp.get_date_info()    ##
    #name_sheet = complete_month[0:3]
    name_sheet = 'Oct'
    print(name_sheet) 
    
    with open('Registro.txt','w+') as d:
            Registro = d.read().splitlines()                                ##
    #sheet = "PRE RENOVACION"                                               ##
                                                                            ##
    ##########################################################################


    ################# Definición de variables Generales ###################

    in_files = os.listdir(os.path.join(os.getcwd(),"Archivos entrada"))
    print(in_files)
    if len(in_files)==0:
        try:
            sp.log_error(error="Carpeta vacia: " + os.path.join(os.getcwd(),"Archivos entrada"))
            #sp.error_mail()
        except:
            pass
        print("Carpeta vacia: " + os.path.join(os.getcwd(),"Archivos entrada"))
        print("Verifique que el archivo a facturar se encuentra en esa carpeta y tiene extensión .xlsx y vuelva a ejecutar")
        time.sleep(100)

    for in_file in in_files:

        if ".xlsx" in in_file and not(in_file.startswith("~")):
            
            principal_file = in_file
            file = openpyxl.load_workbook(os.path.join(os.getcwd(), "Archivos entrada", principal_file)) 
           
            sheet = file[name_sheet]
            #print(sheet)

            max_row = sheet.max_row
            max_colum = sheet.max_column
            #print(max_row)
            #print(max_colum)
            
            for i in range(1,max_colum):
                if sheet.cell(row=1, column=i).value == current_month:
                    break
            #print (i)

            list_r = []
            for j in range(3,max_row+1):
                if sheet.cell(row=j, column=i+2).value == Operacion: #Filtro1
                    for comercial in Comercial:
                        if sheet.cell(row=j, column=i+10).value == comercial:#Filtro 2
                            list_r.append(j)
                            #print(j)
            
            k = 0
            for element in list_r:
                k += 1
                check = True
                Id = sheet.cell(row=element, column=i+7).value
                #print(Id)
                
                for registro in Registro: #Verificar que no esta en la lista de registro
                    if (registro == str(Id)):
                        check = False
                        break

                if check:  
                    Nombre = sheet.cell(row=element, column=i+25).value
                    Correo = sheet.cell(row=element, column=i+24).value
                    Poliza = str(sheet.cell(row=element, column=i+4).value)
                    Tipo = sheet.cell(row=element, column=i+6).value
                    Cantidad = 1
                    for n in range(k, len(list_r)):
                        Id2 = sheet.cell(row=list_r[n], column=i+7).value
                        if Id == Id2:
                            Poliza = Poliza + ', ' + str(sheet.cell(row=list_r[n], column=i+4).value)
                            Cantidad += 1 

                    sp.dataOut('Registro.txt',str(Id))
                    sp.dataOut('Registro.txt',Nombre)
                    sp.dataOut('Registro.txt',str(Cantidad))
                    sp.dataOut('Registro.txt',Poliza)
                    sp.dataOut('Registro.txt',Tipo)
                    sp.dataOut('Registro.txt',Correo)
                    sp.dataOut('Registro.txt','')
                
                with open('Registro.txt') as d:
                    Registro=d.read().splitlines() 


            #----------------Envío de Correos--------------------#
            try:
                for c in range(0,len(Registro),7):
                    
                    nombre_correo = Registro[c+1]
                    cantidad_poliza = Registro[c+2]
                    poliza = Registro[c+3]
                    tipo = Registro[c+4]
                    mail_to = Registro[c+5]

                    if int(cantidad_poliza) > 1:
                        cantidad = 's'
                    else:
                        cantidad = ''
                    
                    print("Enviando email a "+ mail_to)
                    outlook = client.Dispatch('outlook.application')
                    mail = outlook.CreateItem(0)
                    mail.To = mail_to
                    mail.Subject = 'Aviso renovación Pólizas ' + poliza

                    mail.HTMLbody = sp.mail_body_renovacion(sp.get_greeting(),nombre_correo, cantidad , poliza, 'Octubre')
                    
                    if tipo == 'Persona Natural':
                        attachment = os.path.join(os.getcwd(), "Plantillas", 'FCC_052020-PN_unlocked.pdf')
                    else: 
                        attachment = os.path.join(os.getcwd(), "Plantillas", 'FCC_052020-PJ_unlocked.pdf')
                    
                    mail.Attachments.Add(attachment) 
                    mail.Send()
                    time.sleep(10)

            except:
                error = sys.exc_info()
                sp.log_error(error)
                #sp.error_mail()
                print("Se produjo una error inesperado en la fila: "+ c + " de Registro.txt")
                print(error[1])
                time.sleep(100)

except:
    try:
        error = sys.exc_info()
        sp.log_error(error)
        #sp.error_mail()
        print("Se produjo una error inesperado")
        print(error[1])
        time.sleep(100)
    except:
        print("Se produjo un error inesperado")
        pass

